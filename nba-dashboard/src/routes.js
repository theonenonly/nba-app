import Home from './components/Home'
import About from './components/About'
import Header from './components/Header'
import User from './components/user/User'
import UserStart from './components/user/UserStart'
import UserDetails from './components/user/UserDetails'
import UserEdit from './components/user/UserEdit'
import Filter from './components/Filters'

export const routes = [
	{
		path: '',
		component: Home
	},
	{
		path: '/about',
		component: About
	},
	{
		path: '/filter',
		component: Filter
	},
	{
		path: '/user', 
		component: User, 
		children: [
			{ 
				path: '', // /user
				component: UserStart 
			},
			{
					path: ':id', // /user/:id
					component: UserDetails
					// beforeEnter: (to, from, next) => {
					// console.log('inside route setup')
					// next()
			},
			{ 
				path: ':id/edit', //  /user/:id/edit
				component: UserEdit,
				name: 'userEdit'
			}
		]
	},
	{
		path: '*', // redirect anything route not specified to the home page with /
		redirect: '/' 
	}
]

// export const routes = [
// 	{ path: '', component: Home },
// 	{ path: '/user', 
// 		component: User, 
// 		children: [
// 			{ path: '', component: UserStart },
// 			{ path: 'user/:id', component: UserDetails },
// 			{ path: 'user/:id/edit', component: UserEdit }
// 		]
// 	}
// ]
