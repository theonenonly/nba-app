import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VeeValidate from 'vee-validate'
import { routes } from './routes'

Vue.use(VeeValidate)

Vue.use(VueRouter)
// var axios = require('axios')
Vue.use(VueAxios, axios)

// Vue.prototype.$http = Axios

// var VueResource = require('vue-resource') Es5 syntax
// Vue.use(VueResource) 

const dictionary = {
  en: {
	messages: {
		email: function () { 
			return 'Please enter a valid email address'
		},
		required: function (category) {
			if (category === 'email') {
				return 'Please provide an ' + category
			} else {
				return 'Please provide a ' + category
			}
		}
	}
  }
}

VeeValidate.Validator.updateDictionary(dictionary)

const router = new VueRouter({
	routes // same as routes: routes
})

// Navigation Guard - this fn gets executed after every router action 
router.beforeEach((to, from, next) => {
	console.log('global beforeEach setup')
	next()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  router, // equivalent to router: router
  components: { App }
})
